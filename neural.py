import numpy as np

class network():

    def __init__(self):
        # generating weights for network
        np.random.seed(1)
        self.synWeights = 2 * np.random.random((3, 1)) - 1

    def sigmoid(self, x):

        return 1 / (1 + np.exp(-x))
